package com.soldier.steyrAug;

public class RifleMagazine {

    private String brandName = "";
    private String type = "";
    private String caliber = "";
    private int sizeOfMagazine = 30;
    private int currentAmmunitions = 0;
    RifleAmmunition [] ammunitions;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCaliber() {
        return caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public int getSizeOfMagazine() {
        return sizeOfMagazine;
    }

    public void setSizeOfMagazine(int sizeOfMagazine) {
        this.sizeOfMagazine = sizeOfMagazine;
        ammunitions = new RifleAmmunition[sizeOfMagazine];
    }

    public void addAmmunition(RifleAmmunition ammunition) {
        ammunitions[currentAmmunitions] = ammunition;
        currentAmmunitions++;
    }

    public void minusPatron() {
        ammunitions[currentAmmunitions - 1] = null;
        currentAmmunitions--;
    }

    @Override
    public String toString() {
        return "steyrAugMagazine{" +
                "currentAmmunitions=" + currentAmmunitions+
                '}';
    }
}
