package com.soldier.steyrAug;

public class MainRifle {

    public static void main(String[] args) {

        Rifle steyrAug = new Rifle();

        steyrAug.setBrandName("Steyr AUG a3");
        steyrAug.setType("Assault rifle Bullpup");
        steyrAug.setAim("Reflex sight");
        steyrAug.setMagazine(chargeMagazine("Steyr AUG 5,56×45 magazine", "5,56×45 rifle", "5,56×45", 30));

        System.out.println(steyrAug.toString());

        steyrAug.BurstShooting(5);

        System.out.println(steyrAug.toString());

    }
    public static RifleMagazine chargeMagazine(String brandName, String type, String caliber, int size) {
        RifleMagazine magazine = new RifleMagazine();
        magazine.setBrandName(brandName);
        magazine.setType(type);
        magazine.setCaliber(caliber);
        magazine.setSizeOfMagazine(size);

        for (int i = 0; i < magazine.getSizeOfMagazine(); i++) {
            RifleAmmunition ammunitionY = new RifleAmmunition();
            ammunitionY.setBrandName(brandName);
            ammunitionY.setType(type);
            ammunitionY.setCaliber(caliber);

            magazine.addAmmunition(ammunitionY);
        }
        return magazine;
    }
}
