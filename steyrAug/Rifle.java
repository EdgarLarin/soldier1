package com.soldier.steyrAug;

public class Rifle {

    private String brandName = "";
    private String type = "";
    private String aim = "";
    private RifleMagazine magazine;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAim() {
        return aim;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public RifleMagazine getMagazine() {
        return magazine;
    }

    public void setMagazine(RifleMagazine magazine) {
        this.magazine = magazine;
    }

    public void shot (){
        magazine.minusPatron();
    }

    public void tripleShot() {
        shot ();
        shot ();
        shot ();
    }

    public static int numberOfShotsBurstShooting(int longPressOnTheTrigger) {
        int rateOfFire = 3;
        int numberOfShots = longPressOnTheTrigger * rateOfFire;
        return numberOfShots;
    }

    public void BurstShooting( int longPressOnTheTrigger) {
        int numberOfShots = numberOfShotsBurstShooting(longPressOnTheTrigger);
        for (int i = 0; i < numberOfShots; i++) {
            shot();
        }
    }

    @Override
    public String toString() {
        return "Rifle{" +
                "brandName='" + brandName + '\'' +
                ", magazine=" + magazine.toString() +
                '}';
    }
}
