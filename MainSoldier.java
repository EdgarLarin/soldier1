package com.soldier;

import com.soldier.backpack.Backpack;

public class MainSoldier {

    public static void main(String[] args) {

        Soldier soldier = new Soldier();

        soldier.setName("Ivan");
        soldier.setMilitaryRank("Soldier");

        Backpack backpack = new Backpack();

        backpack.setBrandName("Viper");
        backpack.setType("Military Tactical backpack");
    }
}
