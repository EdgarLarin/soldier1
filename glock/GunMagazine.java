package com.soldier.glock;

public class GunMagazine {

    private String brandName = "";
    private String type = "";
    private String caliber = "";
    private int sizeOfMagazine = 17;
    private int currentAmmunitions = 0;
    GunAmmunition[] ammunitions;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCaliber() {
        return caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public int getSizeOfMagazine() {
        return sizeOfMagazine;
    }

    public void setSizeOfMagazine(int sizeOfMagazine) {
        this.sizeOfMagazine = sizeOfMagazine;
        ammunitions = new GunAmmunition[sizeOfMagazine];
    }

    public void addAmmunition(GunAmmunition ammunition) {
        ammunitions[currentAmmunitions] = ammunition;
        currentAmmunitions++;
    }

    public void minusPatron() {
        ammunitions[currentAmmunitions - 1] = null;
        currentAmmunitions--;
    }

    public void print() {
        for (int i = 0; i < currentAmmunitions; i++) {
            GunAmmunition ammunition = ammunitions[i];
            System.out.println(ammunition.getBrandName() + " " + ammunition.getType());
        }
    }

    @Override
    public String toString() {
        return "GunMagazine{" +
                "currentAmmunitions=" + currentAmmunitions+
                '}';
    }
}

