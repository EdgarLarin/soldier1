package com.soldier.glock;

public class GunAmmunition {

    private String brandName = "";
    private String type = "";
    private String caliber = "";

    public String getCaliber() {
        return caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "GunAmmunition{" +
                "brandName='" + brandName + '\'' +
                ", type='" + type + '\'' +
                ", caliber='" + caliber + '\'' +
                '}';
    }
}


