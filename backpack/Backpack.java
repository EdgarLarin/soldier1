package com.soldier.backpack;

import com.soldier.glock.GunMagazine;
import com.soldier.steyrAug.RifleMagazine;

public class Backpack {

    private String brandName = "";
    private String type = "";
    private GunMagazine glockMagazine;
    private RifleMagazine steyrMagazine;
    private int numberOfPocket = 12;
    private int currentGlockMagazine = 0;
    GunMagazine[] glockMagazines;
    private int currentSteyrMagazine = 0;
    RifleMagazine[] rifleMagazines;

    public void setGlockMagazine(GunMagazine glockMagazine) {
        this.glockMagazine = glockMagazine;
    }

    public void setSteyrMagazine(RifleMagazine steyrMagazine) {
        this.steyrMagazine = steyrMagazine;
    }

    public GunMagazine getGlockMagazine() {
        return glockMagazine;
    }

    public int getNumberOfPocket() {
        return numberOfPocket;
    }

    public void addGlockMagazine(GunMagazine glockMagazine) {
        glockMagazines[currentGlockMagazine] = glockMagazine;
        currentGlockMagazine++;
    }

    public RifleMagazine getSteyrMagazine() {
        return steyrMagazine;
    }

    public void addSteyrMagazine(RifleMagazine steyrMagazine) {
        rifleMagazines[currentSteyrMagazine] = steyrMagazine;
        currentSteyrMagazine++;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "backpack{" +
                ", currentGlockMagazine=" + currentGlockMagazine +
                ", currentSteyrMagazine=" + currentSteyrMagazine +
                '}';
    }
}


