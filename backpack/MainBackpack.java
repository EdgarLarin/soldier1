package com.soldier.backpack;

import com.soldier.glock.Gun;
import com.soldier.glock.GunAmmunition;
import com.soldier.glock.GunMagazine;

public class MainBackpack {

    public static void main(String[] args) {

        Backpack backpack = new Backpack();
        backpack.setBrandName("Viper");
        backpack.setType("Military Tactical backpack");

        int numberOfGlockMagazine = 5;
        glock("glock 19 Pro", "pistol", "Reflex sight");

        for (int i = 0; i < numberOfGlockMagazine; i++) {
            backpack.setGlockMagazine(chargedGlockMagazine("glock 19 Pro magazine", "9 × 19 mm Parabellum", "9 × 19 mm", 17));
        }
    }

    /**
     * Loaded magazine for glock
     */
    public static GunMagazine chargedGlockMagazine(String brandName, String type, String caliber, int size) {
        GunMagazine magazine = new GunMagazine();
        magazine.setBrandName(brandName);
        magazine.setType(type);
        magazine.setCaliber(caliber);
        magazine.setSizeOfMagazine(size);

        for (int i = 0; i < magazine.getSizeOfMagazine(); i++) {
            GunAmmunition ammunitionX = new GunAmmunition();
            ammunitionX.setBrandName(brandName);
            ammunitionX.setType(type);
            ammunitionX.setCaliber(caliber);

            magazine.addAmmunition(ammunitionX);
        }
        return magazine;
    }

    /**
     * Create new glock
     */
    public static Gun glock(String brandName, String type, String aim) {
        Gun glock = new Gun();
        glock.setBrandName(brandName);
        glock.setType(type);
        glock.setAim(aim);

        return glock;
    }

}
