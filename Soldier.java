package com.soldier;

import com.soldier.glock.Gun;
import com.soldier.steyrAug.Rifle;

public class Soldier {

    private String name = "";
    private String militaryRank = "";
    private Gun glock;
    private Rifle steyrAug;

    public Gun getGlock() {
        return glock;
    }

    public void setGlock(Gun glock) {
        this.glock = glock;
    }

    public Rifle getSteyrAug() {
        return steyrAug;
    }

    public void setSteyrAug(Rifle steyrAug) {
        this.steyrAug = steyrAug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMilitaryRank() {
        return militaryRank;
    }

    public void setMilitaryRank(String militaryRank) {
        this.militaryRank = militaryRank;
    }

//    public void glockSinglePressingTrigger() {
//        glock.shot();
//    }
//
//    public void glockTripleShotPressingTrigger() {
//        glock.shot();
//        glock.shot();
//        glock.shot();
//    }
//
//    public void glockBurstShootingPressingTrigger(int longPressOnTheTrigger) {
//        glock.burstShooting(longPressOnTheTrigger);
//    }
//
//    public void steyrAugSinglePressingTrigger() {
//        steyrAug.shot();
//    }
//
//    public void steyrAugTripleShotPressingTrigger() {
//        steyrAug.shot();
//        steyrAug.shot();
//        steyrAug.shot();
//    }
//
//    public void steyrAugBurstShootingPressingTrigger(int longPressOnTheTrigger) {
//        steyrAug.BurstShooting(longPressOnTheTrigger);
//    }

}

// soldiers should just pull the trigger, but he also must be able to switch other mode of fire
// or methods should be something like :
// public void shootSingle()
// public void shootTriple()