package com.soldier.colt;

public class GunMagazine2 {

    private String brandName = "";
    private String type = "";
    private String caliber = "";
    private int sizeOfMagazine = 7;
    private int currentAmmunitions = 0;
    GunAmmunition2[] ammunitions2;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCaliber() {
        return caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public int getSizeOfMagazine() {
        return sizeOfMagazine;
    }

    public void setSizeOfMagazine(int sizeOfMagazine) {
        this.sizeOfMagazine = sizeOfMagazine;
    }

    public int getCurrentAmmunitions() {
        return currentAmmunitions;
    }

    public void setCurrentAmmunitions(int currentAmmunitions) {
        this.currentAmmunitions = currentAmmunitions;
    }

    public GunAmmunition2[] getAmmunitions2() {
        return ammunitions2;
    }

    public void setAmmunitions2(GunAmmunition2[] ammunitions2) {
        this.ammunitions2 = ammunitions2;
    }

}
